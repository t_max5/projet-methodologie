# Projet M3301



## Authors
Groupe G5-3 : Benoit Chevillon, Fabien Hannon, Rayane Ben Ahmed, Lucas Phillipe, Cyril Blanchard, Adrian Bednarowicz, Matteo de la Fuente, Julien Leveneur, Valentin François



## Cloning
### Afin de cloner le dépôt GitLab sur son poste de travail une 1ère fois :
- A l'aide de Git Bash, se positionner dans le répertoire désiré qui contiendra le dossier du dépôt.
- Exécuter la commande "git clone https://gitlab.com/t_max5/projet-methodologie.git" pour télécharger le projet.
### Afin de mettre à jour le dépôt déjà cloné auparavant :
- A l'aide de Git Bash, se positionner dans le dossier du dépôt.
- Exécuter la commande "git pull".