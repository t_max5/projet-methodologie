// Check the 22/12 - Name : Benoit CHEVILLON
//Comments : Cette fonction va calculer des statistiques élémentaires sur le groupe : la moyenne, l’écart-type, la médiane, le minimum et le maximum.
//IN : -
//OUT : dataStats
function Stats_Group() {
  dataNumberMarks = SheetsReading_StudentMarks();
  if (dataNumberMarks.length > 0) { // Vérification que les notes finales ont bien été calculées
     avg = Avg_Marks(dataNumberMarks);
    stdDeviation = StandardDeviation_Marks(dataNumberMarks, avg);
    median = Median_Marks(dataNumberMarks);
    dataMinMax = MinMax_Marks(dataNumberMarks);
    dataStats = [dataNumberMarks,avg, stdDeviation, median, dataMinMax];
    Export_Reporting(dataStats); // Passage de FP3 à FP4
  }
  else
    SpreadsheetApp.getUi().alert("Les notes finales doivent avoir été calculées.");
}


// Check the 22/12 - Name : Benoit CHEVILLON
//Comments : Le but de cette fonction est de récupérer les notes finales sous formes de de nombres depuis la feuille de calcul Sheets.
//IN : -
//OUT : dataNumberMarks
function SheetsReading_StudentMarks() {
  numMark = 0;
  var dataNumberMarks = [];
  while ((reading = sheet.getRange(numMark+6, 12).getValue()) != "") { // Pour chaque note de la colonne "note finale"
    dataNumberMarks[numMark] = reading;
    numMark++;
  }
  return dataNumberMarks;
}


// Check the 22/12 - Name : Benoit CHEVILLON
//Comments : L’objectif de cette fonction est de calculer la moyenne des notes finales du groupe.
//IN : dataNumberMarks
//OUT : avg
function Avg_Marks(dataMarks) {
  length = dataMarks.length;
  sum = 0;
  dataMarks.forEach(function (mark) {
    sum = sum + mark;
  });
  avg = sum/length;
  return avg;
}


// Check the 22/12 - Name : Fabien HANNON
//Comments : Le but de cette fonction est de calculer l’écart-type des notes finales du groupe.
//IN : dataNumberMarks, avg
//OUT : stDeviation
function StandardDeviation_Marks(dataMarks,avg){ // racine(somme((mark - avg)²)/length)
    var length = dataMarks.length;
    var sum = 0;
    dataMarks.forEach(function(mark){
        sum = sum + Math.pow(mark - avg,2);
    });
    var stDeviation = Math.sqrt(sum/length);
    return stDeviation;
}


// Check the 22/12 - Name : Fabien HANNON
//Comments : Le but de cette fonction est de calculer la médiane des notes finales du groupe.
//IN : dataNumberMarks
//OUT : median
function Median_Marks(dataMarks){
    var length = dataMarks.length;
    var median;
    dataMarks = dataMarks.sort(function(a, b) {return a - b;}); // Triage du tableau
    if(length % 2 != 0)
        median = dataMarks[(length-1)/2];
    else
        median = 0.5*(dataMarks[length] + dataMarks[length+1])
    return median;
}


// Check the 22/12 - Name : Fabien HANNON
//Comments : Le but de cette fonction est de donner la plus petite ainsi que la plus grande des notes finale du groupe.
//IN : dataNumberMarks
//OUT : dataMinMax
function MinMax_Marks(dataMarks){
    var min= dataMarks[0];
    var max = dataMarks[0];
    dataMarks.forEach(function(mark){
        if(mark < min)
            min = mark;
        if(mark > max)
            max = mark;
    })
    dataMinMax = [min,max];
    return dataMinMax;
}