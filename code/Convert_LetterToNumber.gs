var ss = SpreadsheetApp.getActiveSpreadsheet();
var sheet = ss.getSheets()[0];

// Check the 22/12 - Name : Fabien HANNON
//Comments : Cette fonction va permettre de convertir les notes, auparavant en format lettre, en format numérique.
//IN : -
//OUT : -
function Convert_LetterToNumber() {
  dataLettersMarks = SheetsReading_LetterMarks();
  if (dataLettersMarks.length > 0) { // Vérification qu'il existe au moins une évaluation
    dataCalculated = Calculation_LetterToNumber(dataLettersMarks);
    SheetsInsertion_NumberMarks(dataCalculated);
  }
  else
    SpreadsheetApp.getUi().alert("Au moins une évaluation est nécessaire.");
}


// Check the 22/12 - Name : Benoit CHEVILLON
//Comments : Le but de cette fonction est de lire pour chaque élève toutes les notes de chaque évaluation sur le document Sheets, ainsi que leurs coefficients.
//IN : -
//OUT : dataLettersMarks
function SheetsReading_LetterMarks() {
  var sheet = SpreadsheetApp.getActiveSheet();
  numExam = 0;
  var data = [];
  var nbStudent = sheet.getRange("K4").getValue();
  while ((numExam < 10) && ((reading = sheet.getRange(5, numExam+3).getValue()) != "")) { // Pour chaque évaluation présente dans le Sheets
    data.push([]);
    coef = reading.toString().split("Coef : ")[1].split("\n")[0]; // Extraction du coefficient de la chaine de caractère
    data[numExam][0] = coef;
    for (var numStudent = 0; numStudent < nbStudent; numStudent++) {
      data[numExam][numStudent+1] = sheet.getRange(numStudent+6,numExam+3).getValue(); // Récupération de la note (lettre)
    }
    numExam++;
  }
  return data;
}


// Check the 22/12 - Name : Benoit CHEVILLON
//Comments : Le but de cette fonction est de convertir l’ensemble des lettres attribuées à chaque étudiant en intervalle et en une note finale.
//IN : dataLettersMarks
//OUT : dataCalculated
function Calculation_LetterToNumber(dataLettersMarks) {
  nbExam = dataLettersMarks.length
  nbStudent = dataLettersMarks[0].length - 1; // Réduction de 1 car le tableau contient également les coefficients
  sumCoef = 0;
  var dataIntervalLower = [];
  var dataIntervalHigh = [];
  var dataNumberMarks  = [];
  for(var exam = 0; exam < nbExam; exam++)
    sumCoef = sumCoef + Number(dataLettersMarks[exam][0]);
  for (var student = 0; student < nbStudent; student++)
    dataIntervalLower[student] = dataIntervalHigh[student] = 0; // Initialisation des tableaux
  for(var student = 1; student < nbStudent + 1; student++) {
    for(var exam = 0; exam < nbExam; exam++) {
      coef = dataLettersMarks[exam][0];
      switch(dataLettersMarks[exam][student]) { // Assignation des bornes de l'intervalle
        case 'A':
          lowerMark = 15;
          highMark = 20;
          break;
        case 'B':
          lowerMark = 10;
          highMark = 14;
          break;
        case 'C':
          lowerMark = 5;
          highMark = 9;
          break;
        case 'D':
          lowerMark = 0;
          highMark = 4;
          break;
      }
      dataIntervalLower[student-1] = dataIntervalLower[student-1] + lowerMark * coef;
      dataIntervalHigh[student-1] = dataIntervalHigh[student-1] + highMark * coef;
    }
    dataNumberMarks[student-1] = ((dataIntervalLower[student-1] + dataIntervalHigh[student-1])/2) / sumCoef; //Calcul de la note finale
    dataIntervalLower[student-1] = dataIntervalLower[student-1] / sumCoef; // Remise des bornes des intervalles entre 0 et 20
    dataIntervalHigh[student-1] = dataIntervalHigh[student-1] / sumCoef;
  }
  dataCalculated = [dataNumberMarks, dataIntervalLower, dataIntervalHigh];
  return dataCalculated;
}


// Check the 22/12 - Name : Benoit CHEVILLON
//Comments : Le but de cette fonction est d’insérer les valeurs des notes chiffrées calculées ainsi que les bornes des intervalles dans la page Sheets.
//IN : dataCalculated
//OUT : -
function SheetsInsertion_NumberMarks(dataCalculated) {
  for (var numMark = 0; numMark < dataCalculated[0].length; numMark++) { // Pour chaque étudiant
    sheet.getRange(numMark+6, 10).setValue(dataCalculated[1][numMark]); //+6 car la première note est ligne 6
    sheet.getRange(numMark+6, 11).setValue(dataCalculated[2][numMark]);
    sheet.getRange(numMark+6, 12).setValue(dataCalculated[0][numMark]);
  }
}