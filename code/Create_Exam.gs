var ss = SpreadsheetApp.getActiveSpreadsheet();
var sheet = ss.getSheets()[0];

// Check the 22/12 - Name : Benoit CHEVILLON
//Comments : Cette fonction permet la saisie des données nécessaire à l’ajout d’une évaluation sur le document sheet : nom, date et coefficient.
//IN : dataConfigExam
//OUT : -
function Create_Exam(dataConfigExam){
  if (dataConfigExam == null) // Légère divergence avec l'AD en raison du fonctionnement spécifique de G Apps Script
    Config_Exam();
  else
    SheetsInsertion_Exam(dataConfigExam);
}


// Check the 22/12 - Name : Fabien HANNON
//Comments : Le but de cette fonction est de faire saisir les données nécessaires à la configuration de la nouvelle évaluation, autrement dit le nom, la date et le coefficient de l’évaluation.
//IN : -
//OUT : dataConfigExam
function Config_Exam() {
  var ui = SpreadsheetApp.getUi();
  var tmp = HtmlService.createTemplateFromFile('Create_Exam_Sidebar');
  var html = tmp.evaluate();
  html.setTitle("Ajout d'une évaluation");
  ui.showSidebar(html); // Ouverture de la barre latérale, continuité de la fonction dans le fichier .html
}


// Check the 22/12 - Name : Benoit CHEVILLON
//Comments : Cette fonction s’occupe d’insérer les données précédemment saisies liées à la configuration de la nouvelle évaluation dans Sheets.
//IN : dataConfigExam
//OUT : -
function SheetsInsertion_Exam(dataExam) {
  var exam = dataExam[1] + " | Coef : " + dataExam[2] + "\n" + dataExam[0] ; // Construction du label de l'évaluation
  var numLigne = 3;
  while (numLigne<10 && sheet.getRange(5, numLigne).getValue() != "") // Comptage du nombre d'évaluation actuel
    numLigne++
  if (!(numLigne < 10))
    SpreadsheetApp.getUi().alert("Nombre maximum d'évaluation atteint.");
  else
    sheet.getRange(5, numLigne).setValue(exam);
}