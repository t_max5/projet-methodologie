var ss = SpreadsheetApp.getActiveSpreadsheet();
var sheet = ss.getSheets()[0];
var newSpreadSheets;
var newSheet;

// Check the 23/12 - Name : Benoit CHEVILLON
//Comments : Le but de cette fonction est d’exporter les données récoltées dans le système de gestion et de reporting dans un fichier PDF. Un histogramme de distribution des notes finales sera également créé.
//IN : dataStats
//OUT : -
function Export_Reporting(dataStats) {
  Create_SheetsToPDF();
  Copy_SheetsToPDF();
  WriteStats_SheetsToPDF(dataStats)
  Generate_HistogramToPDF(dataStats);
  Generate_PDF();
}


// Check the 23/12 - Name : Benoit CHEVILLON
//Comments : Le but de cette fonction est de créer un document Sheets temporaire à partir de celui actuel qui sera modifié afin de correspondre aux données nécessaires sur le bilan PDF.
//IN : -
//OUT : -
function Create_SheetsToPDF() {
  today = new Date();
  var dateToday = 'Gestion de notes ' + today.getDate() + "/" + (today.getMonth() + 1) + "/" + today.getFullYear();
  dateToday = dateToday + " - " + (today.getHours()+6) + "H" + today.getMinutes(); // Obtention du nom du fichier
  newSpreadSheets = ss.copy('Gestion de notes ' + dateToday); // Copie du fichier Sheets actuel
  newSheet = newSpreadSheets.getSheets()[0];
  var parents = DriveApp.getFileById(newSpreadSheets.getId()).getParents();
  if (parents.hasNext()) // Obtention de l'emplacement du fichier temporaire
    var folder = parents.next(); 
  else
    folder = DriveApp.getRootFolder();
  var newSheetsReference = DriveApp.getFileById(newSpreadSheets.getId());
  folder.addFile(newSheetsReference); // Ajout du fichier temporaire dans le dossier drive
}


// Check the 23/12 - Name : Benoit CHEVILLON
//Comments : Le but de cette fonction est de recopier, déplacer et adapter les données actuelles présentes dans le document Sheets vers le document permettant la génération du PDF.
//IN : -
//OUT : -
function Copy_SheetsToPDF() {
  var numColumn = 9;
  newSheet.getRange("L4").setValue(""); // Suppression de mises en formes inutiles
  newSheet.getRange("J4:L4").setBackground("white");
  while (newSheet.getRange(5, numColumn).getValue() == "") { // Suppression des colonnes vides ne contenant pas d'évaluation
    newSheet.deleteColumn(numColumn);
    numColumn--;
  }
  newSheet.getDrawings().forEach(function(drawing) {drawing.remove();}) // Suppression des boutons de gestion
  newSheet.getRange("C2:D3").merge();
  var titre = newSheet.getRange("C2"); // Configuration d'un titre dans le document
  titre.setValue("Bilan des notes");
  titre.setFontStyle("bold");
  titre.setFontSize("18");
  titre.setHorizontalAlignment("center");
  titre.setVerticalAlignment("middle");
  newSheet.autoResizeRows(2, 3);
}


// Check the 23/12 - Name : Fabien HANNON
//Comments : Le but de cette fonction est d’écrire les données statistiques précédemment calculées sur le document permettant la génération du fichier PDF.
//IN : dataStats
//OUT : -
function WriteStats_SheetsToPDF(dataStats){
  var stats = newSheet.getRange(dataStats[0].length,dataStats[0].length,dataStats[0].length+15); // Calcul des emplacements d'écriture
  stats.setNumberFormat("@"); // Afin d'éviter que des nombres ne soient considérés comme des dates
  newSheet.getRange(dataStats[0].length+7, 1).setValue("Moyenne :"); // Ecriture des différents statistiques calculés
  newSheet.getRange(dataStats[0].length+7, 2).setValue(dataStats[1].toFixed(2));
  newSheet.getRange(dataStats[0].length+8, 1).setValue("Médiane :");
  newSheet.getRange(dataStats[0].length+8, 2).setValue(dataStats[3].toFixed(2));
  newSheet.getRange(dataStats[0].length+9, 1).setValue("Ecart-type :");
  newSheet.getRange(dataStats[0].length+9, 2).setValue(dataStats[2].toFixed(2));
  newSheet.getRange(dataStats[0].length+10, 1).setValue("Note minimale :");
  newSheet.getRange(dataStats[0].length+10, 2).setValue(dataStats[4][0].toFixed(2));
  newSheet.getRange(dataStats[0].length+11, 1).setValue("Note maximale :");
  newSheet.getRange(dataStats[0].length+11, 2).setValue(dataStats[4][1].toFixed(2));
  newSheet.autoResizeColumns(1, 15);
}


// Check the 23/12 - Name : Fabien HANNON
//Comments : Le but de cette fonction est de générer l’histogramme basé sur les notes saisies dans le document Sheets.
//IN : dataStats
//OUT : -
function Generate_HistogramToPDF(dataStats){
  var newSheet2 = newSpreadSheets.getSheets()[1];
  for(var i = 0; i < dataStats[0].length;i++)
    dataStats[0][i] = dataStats[0][i].toFixed(); // Toutes les notes sont arrondis afin d'être gérées dans le graphique
  for (var numMark = 1; numMark <= dataStats[0].length; numMark++)
    newSheet2.getRange(numMark, 1).setValue(dataStats[0][numMark-1]); // Ecriture de toutes les valeurs sur la feuille 2, servant au graphique
  var textStyleBuilder = Charts.newTextStyle().setFontName('Open Sans').setColor('#4285F4').setFontSize(26); // Configuration du style
  var style = textStyleBuilder.build();
  var newChart = newSheet.newChart().setChartType(Charts.ChartType.BAR).asColumnChart()
    .setTitle('Représentation des notes').setTitleTextStyle(style)
    .addRange(newSheet2.getRange("C1:D20")).setOption("useFirstColumnAsDomain", true) // Définition de la plage de données utilisée (feuille 2)
    .setPosition(dataStats[0].length+7, 3, 0, 0)
    .setOption('vAxes.0.title', 'Notes / Occurence').setOption("hAxis", {title: "Individu / notes"})
    .build();
    newSheet.insertChart(newChart);
}


// Check the 23/12 - Name : Benoit CHEVILLON
//Comments : Le but de cette fonction est de générer complètement le document PDF afin qu’il soit accessible à l’utilisateur. Un emplacement et un nom précis sont définis.
//IN : -
//OUT : -
function Generate_PDF() {
  today = new Date();
  var pdfName = 'Gestion de notes ' + today.getDate() + "/" + (today.getMonth() + 1) + "/" + today.getFullYear();
  var pdfName = pdfName + " - " + (today.getHours()+6) + "H" + today.getMinutes(); // Obtention du nom du fichier
  var parents = DriveApp.getFileById(newSpreadSheets.getId()).getParents();
  if (parents.hasNext()) // Obtention de l'emplacement du fichier PDF
    var folder = parents.next(); 
  else
    folder = DriveApp.getRootFolder();
  newSpreadSheets.getSheets()[1].hideSheet(); // La feuille 2 est masquée pour l'exportation PDF
  var theBlob = newSpreadSheets.getBlob().getAs('application/pdf').setName(pdfName); // Exportation PDF
  var newFile = folder.createFile(theBlob);
  DriveApp.getFileById(newSpreadSheets.getId()).setTrashed(true); // Suppression du fichier temporaire
  SpreadsheetApp.getUi().alert("Votre PDF '" + pdfName + "' a bien été généré à l'emplacement '" + folder + "'.");
}